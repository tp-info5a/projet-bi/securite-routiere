-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 05 fév. 2021 à 12:14
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : securite_routiere
--
CREATE DATABASE IF NOT EXISTS securite_routiere DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE securite_routiere;

-- --------------------------------------------------------

--
-- Structure de la table accident_fact
--

CREATE TABLE accident_fact (
  id varchar(255) NOT NULL,
  lieu_id varchar(255) DEFAULT NULL,
  voie_id varchar(255) DEFAULT NULL,
  date_id varchar(50) DEFAULT NULL,
  meteo_dim int(11) DEFAULT NULL,
  intersection_id int(11) DEFAULT NULL,
  surf varchar(255) DEFAULT NULL,
  situation varchar(255) DEFAULT NULL,
  luminosite varchar(255) DEFAULT NULL,
  heure int(11) DEFAULT NULL,
  nombre_morts int(11) DEFAULT 0,
  nombre_blesses int(11) DEFAULT 0,
  nombre_hospitalises int(11) DEFAULT 0,
  nombre_indemmes int(11) DEFAULT 0,
  pourcentage_homme int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table date_dim
--

CREATE TABLE date_dim (
  id varchar(50) NOT NULL,
  jour int(11) NOT NULL DEFAULT 1,
  mois int(11) NOT NULL DEFAULT 1,
  trimestre int(11) NOT NULL DEFAULT 1,
  annee int(11) NOT NULL DEFAULT 1970,
  vacances tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table intersection_dim
--

CREATE TABLE intersection_dim (
  id int(11) NOT NULL,
  type varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table lieu_dim
--

CREATE TABLE lieu_dim (
  id varchar(255) NOT NULL,
  commune varchar(250) DEFAULT NULL,
  agglomeraton varchar(255) DEFAULT NULL,
  departement varchar(255) DEFAULT NULL,
  region varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table manoeuvre_dim
--

CREATE TABLE manoeuvre_dim (
  id int(11) NOT NULL,
  description varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table meteo_dim
--

CREATE TABLE meteo_dim (
  id int(11) NOT NULL,
  description varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table obstacle_fixe_dim
--

CREATE TABLE obstacle_fixe_dim (
  id int(11) NOT NULL,
  description varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table obstacle_mobile_dim
--

CREATE TABLE obstacle_mobile_dim (
  id int(11) NOT NULL,
  description varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table vehicule_dim
--

CREATE TABLE vehicule_dim (
  id int(10) NOT NULL,
  categorie varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table vehicule_fact
--

CREATE TABLE vehicule_fact (
  vehicule_id varchar(255) NOT NULL,
  meteo_id int(11) DEFAULT NULL,
  obstacle_fixe_id int(11) DEFAULT NULL,
  obstacle_mobile_id int(11) DEFAULT NULL,
  manoeuvre_id int(11) DEFAULT NULL,
  zone_choc_id int(11) DEFAULT NULL,
  cat_vehicule_id int(11) DEFAULT NULL,
  nombre_morts int(11) DEFAULT NULL,
  nombre_blesses int(11) DEFAULT NULL,
  nombre_hospitalises int(11) DEFAULT NULL,
  nombre_indemmes int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table voie_dim
--

CREATE TABLE voie_dim (
  id varchar(255) NOT NULL,
  categorie varchar(255) DEFAULT NULL,
  nombre int(11) DEFAULT 1,
  type_circulation varchar(255) DEFAULT NULL,
  profil_route varchar(255) DEFAULT NULL,
  plan_route varchar(255) DEFAULT NULL,
  largeur_route int(11) DEFAULT NULL,
  infrastructure varchar(255) DEFAULT NULL,
  proximite_ecole tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table zone_choc_dim
--

CREATE TABLE zone_choc_dim (
  id int(11) NOT NULL,
  description varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table accident_fact
--
ALTER TABLE accident_fact
  ADD PRIMARY KEY (id),
  ADD KEY accident_fact_lieu_dim_id_fk (lieu_id),
  ADD KEY accident_fact_voie_dim_id_fk (voie_id),
  ADD KEY accident_fact_date_dim_id_fk (date_id),
  ADD KEY accident_fact_meteo_dim_id_fk (meteo_dim),
  ADD KEY accident_fact_intersection_dim_id_fk (intersection_id);

--
-- Index pour la table date_dim
--
ALTER TABLE date_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table intersection_dim
--
ALTER TABLE intersection_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table lieu_dim
--
ALTER TABLE lieu_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table manoeuvre_dim
--
ALTER TABLE manoeuvre_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table meteo_dim
--
ALTER TABLE meteo_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table obstacle_fixe_dim
--
ALTER TABLE obstacle_fixe_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table obstacle_mobile_dim
--
ALTER TABLE obstacle_mobile_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table vehicule_dim
--
ALTER TABLE vehicule_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table vehicule_fact
--
ALTER TABLE vehicule_fact
  ADD PRIMARY KEY (vehicule_id),
  ADD KEY vehicule_fact_manoeuvre_dim_id_fk (manoeuvre_id),
  ADD KEY vehicule_fact_meteo_dim_id_fk (meteo_id),
  ADD KEY vehicule_fact_obstacle_fixe_dim_id_fk (obstacle_fixe_id),
  ADD KEY vehicule_fact_vehicule_dim_id_fk (cat_vehicule_id),
  ADD KEY vehicule_fact_zone_choc_dim_id_fk (zone_choc_id),
  ADD KEY vehicule_fact_obstacle_mobile_dim_id_fk (obstacle_mobile_id);

--
-- Index pour la table voie_dim
--
ALTER TABLE voie_dim
  ADD PRIMARY KEY (id);

--
-- Index pour la table zone_choc_dim
--
ALTER TABLE zone_choc_dim
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table intersection_dim
--
ALTER TABLE intersection_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table manoeuvre_dim
--
ALTER TABLE manoeuvre_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table meteo_dim
--
ALTER TABLE meteo_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table obstacle_fixe_dim
--
ALTER TABLE obstacle_fixe_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table obstacle_mobile_dim
--
ALTER TABLE obstacle_mobile_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table zone_choc_dim
--
ALTER TABLE zone_choc_dim
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table accident_fact
--
ALTER TABLE accident_fact
  ADD CONSTRAINT accident_fact_date_dim_id_fk FOREIGN KEY (date_id) REFERENCES date_dim (id),
  ADD CONSTRAINT accident_fact_intersection_dim_id_fk FOREIGN KEY (intersection_id) REFERENCES intersection_dim (id),
  ADD CONSTRAINT accident_fact_lieu_dim_id_fk FOREIGN KEY (lieu_id) REFERENCES lieu_dim (id),
  ADD CONSTRAINT accident_fact_meteo_dim_id_fk FOREIGN KEY (meteo_dim) REFERENCES meteo_dim (id),
  ADD CONSTRAINT accident_fact_voie_dim_id_fk FOREIGN KEY (voie_id) REFERENCES voie_dim (id);

--
-- Contraintes pour la table vehicule_fact
--
ALTER TABLE vehicule_fact
  ADD CONSTRAINT vehicule_fact_manoeuvre_dim_id_fk FOREIGN KEY (manoeuvre_id) REFERENCES manoeuvre_dim (id),
  ADD CONSTRAINT vehicule_fact_meteo_dim_id_fk FOREIGN KEY (meteo_id) REFERENCES meteo_dim (id),
  ADD CONSTRAINT vehicule_fact_obstacle_fixe_dim_id_fk FOREIGN KEY (obstacle_fixe_id) REFERENCES obstacle_fixe_dim (id),
  ADD CONSTRAINT vehicule_fact_obstacle_mobile_dim_id_fk FOREIGN KEY (obstacle_mobile_id) REFERENCES obstacle_mobile_dim (id),
  ADD CONSTRAINT vehicule_fact_vehicule_dim_id_fk FOREIGN KEY (cat_vehicule_id) REFERENCES vehicule_dim (id),
  ADD CONSTRAINT vehicule_fact_zone_choc_dim_id_fk FOREIGN KEY (zone_choc_id) REFERENCES zone_choc_dim (id);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
