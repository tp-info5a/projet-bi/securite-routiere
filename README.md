# Sécurité Routière

## Membre du groupe

ABDOU-RAHIMI Nassère <br>
ARMANET Nathan <br>
CAZET Martin <br>
MARTELIN Félix <br>
SARTEUR Thomas <br>

## Lien vers les fichiers

https://www.data.gouv.fr/fr/datasets/bases-de-donnees-annuelles-des-accidents-corporels-de-la-circulation-routiere-annees-de-2005-a-2019/

## Liste des KPI

- Quel est l'impact des conditions métérologique sur les accident ?
- Quels sont les départements/régions les plus touchés ?
- Le type d'intersection joue-t-il sur les accidents ?
- Le type de véhicule influe-t-il sur la gravité des accidents ?
- L'âge du conducteur joue-t-il sur les accidents ?
- Le sexe du conducteur joue-t-il sur les accidents ?
- Quels sont les catégories d'usagers les plus touché par les accidents ?
- La luminosité a-t-elle un effet sur le nombre d'accidents ?
- Y a-t-il une différence entre les zones rurals et urbaines sur la gravité d'accidents ?

## Scéma en étoile

![schéma en étoile](./schema-etoile.png)