package routines;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class Surface {

    /**
     */
    public static String EtatSurface(int etat) {
        switch(etat) {
        case 1 :
        	return "normale";
        case 2 :
        	return "mouillée";
        case 3 :
        	return "flaques";
        case 4 :
        	return "inondée";
        case 5 :
        	return "enneigée";
        case 6 :
        	return "boue";
        case 7 :
        	return "verglacée";
        case 8 :
        	return "corps gras - huile";
        case 9 :
        	return "autre";
        default :
        	return "non précisé";
        }
    }
    
    public static String Situation(int sit) {
        switch(sit) {
        case 1 :
        	return "Sur chaussée";
        case 2 :
        	return "Sur bande d'arrêt d'urgence";
        case 3 :
        	return "Sur accotement";
        case 4 :
        	return "Sur trottoir";
        case 5 :
        	return "Sur piste cyclable";
        default :
        	return "non précisé";
        }
    }
    
    public static String Luminosite(int lum) {
        switch(lum) {
        case 1 :
        	return "Plein jour";
        case 2 :
        	return "Crépuscule ou aube";
        case 3 :
        	return "Nuit sans éclairage public";
        case 4 :
        	return "Nuit avec éclairage public non allumé";
        case 5 :
        	return "Nuit avec éclairage public allumé";
        default :
        	return "non précisé";
        }
    }
    
    public static Date ToDate(int year,int month, int day) {
    	String date = String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(day);
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY/MM/DD");
        Date dates = null;
        try {
			dates = simpleDateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return dates;
    }
}
